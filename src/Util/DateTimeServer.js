
const url = "https://ginco.site"
/**
 * Módulo responsável por prover as constants do sistema
 */
class DateTimeServer {

  static getServerTime() {
    let xmlHttp;

    try {
      //FF, Opera, Safari, Chrome
      xmlHttp = new XMLHttpRequest();
    }
    catch (err1) {
      //IE
      try {
        xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
      }
      catch (err2) {
        try {
          xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
        }
        catch (eerr3) {
          //AJAX not supported, use CPU time.
          alert("AJAX not supported");
        }
      }
    }
    xmlHttp.open('HEAD', url, false);
    xmlHttp.setRequestHeader("Content-Type", "text/html");
    xmlHttp.send('');
    return xmlHttp.getResponseHeader("Date");
  }
}

export default DateTimeServer;

