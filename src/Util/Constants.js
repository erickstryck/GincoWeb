import React, { Component } from 'react';

/**
 * Módulo responsável por prover as constants do sistema
 */
class Constants extends Component {

  constructor() {
    super();
    this.constants = {

      //Errors
      CENTRAL_COMMAND: "CENTRAL_COMMAND",
      EMAIL_RESEND_FAILURE: "EMAIL_RESEND_FAILURE",
      DATE_EVENT_INVALID: "DATE_EVENT_INVALID",
      INVALID_ACCESS_KEY: 'INVALID_ACCESS_KEY',

      //Error Google Auth Codes
      WRONG_PASSWORD: 'auth/wrong-password',
      USER_NOT_FOUND: 'auth/user-not-found',
      EMAIL_IN_USE: 'auth/email-already-in-use',
      WEAK_PASSWORD: 'auth/weak-password',
      REMOTE_ARCHIVE_FAIL: 'storage/object-not-found',
      EMAIL_IN_USE_ACCOUNT: 'auth/account-exists-with-different-credential',

      //Languages
      PT_BR: {
        //Messages error
        MSG_INVALID_PASSWORD: 'A senha é inválida ou o usuário não possui uma senha.',
        MSG_WEAK_PASSWORD: 'A senha deve possuir entre 6 e 16 caracteres.',
        MSG_WEAK_NAME: 'O nome deve possuir entre 6 e 40 caracteres.',
        MSG_WEAK_TEAM_NAME: 'O nome deve possuir até 23 caracteres.',
        MSG_WEAK_TEAM_WARCRY: 'O grito de guerra deve possuir até 76 caracteres.',
        MSG_WEAK_STUDENT_NAME: 'O nome do aluno deve possuir até 30 caracteres.',
        MSG_WEAK_THEME_NAME: 'O tema deve possuir entre 6 e 40 caracteres.',
        MSG_INVALID_USER: 'O usuário não existe, ou pode ter sido excluído.',
        MSG_REMOTE_ARCHIVE_FAIL: 'Houve um problema ao recuperar alguns arquivos remotos',
        MSG_DATE_EVENT_INVALID: 'A data de realização ou entrega das perguntas é inválida!',
        MSG_INVALID_IMG: 'A imagem precisa estar em um formato PNG ou JPEG.',
        MSG_INVALID_IMG_SIZE: 'O tamanho da imagem deve ser de até 2MB!',
        MSG_WEAK_QUESTION: 'Por favor insira um enunciado com até 178 caracteres',
        MSG_WEAK_ALTERNATIVE: 'Por favor insira uma alternativa com até 120 caracteres',
        MSG_WEAK_NOTE: 'Por favor insira uma observação com até 200 caracteres',
        MSG_FATAL_ERROR: 'O sistema está se recuperando de um erro fatal!',
        MSG_NOTE_TEAM: 'Não se esqueça de informar todos os dados da equipe!',
        MSG_NO_BROWSER_SUPPORT: 'Infelizmente não damos suporte aos seguintes navegadores',

        //Labels
        LABEL_GINCO: 'Ginco',
        LABEL_LOGIN_BUTTON: 'Login',
        LABEL_LOGIN_TITLE: 'Sistemas de gincanas educativas',
        LABEL_FOOTER: 'GINCO 2018',
        LABEL_PATH_TEACHER: 'Sou um professor',
        LABEL_PATH_TEAM: 'Participo de uma equipe',
        LABEL_PATH_REGISTER: 'Quero me registrar',

        LABEL_TEAM_NAME: 'Nome da equipe',
        LABEL_WARCRY: 'Grito de guerra',
        LABEL_UPDATE_TEAM: 'Atualizar Equipe',

        LABEL_PATH_TEAM: 'Participo de uma equipe',
        LABEL_QUESTIONS: 'Perguntas',
        LABEL_VALID_TEAM: 'Esta equipe é válida',
        LABEL_EMPTY_QUESTION: 'Pergunta ainda não criada',
        LABEL_EMPTY: 'Vazio',

        LABEL_HOME: 'Início',
        LABEL_ACCOUNT: 'Meus Dados',
        LABEL_HOME_TITLE: 'Estas são suas gincanas',
        LABEL_DATA_REALIZACAO: 'Data Realização',
        LABEL_TEMA: 'Tema',
        LABEL_ACAO: 'Ação',
        LABEL_SEARCH_DATA_REALIZACAO: 'Procurar Data',
        LABEL_SEARCH_TEMA: 'Procurar Tema',
        LABEL_SEARCH_NAME: 'Procurar Nome',
        LABEL_SEARCH: 'Procurar',
        LABEL_EVENT_DATE: 'Data Realização',
        LABEL_LIMIT_EVENT_DATE: 'Prazo para entrega das perguntas',
        LABEL_TEAM_MANAGER: 'Gerenciar Equipes',
        LABEL_EXPORT_DATA: 'Exportar Dados',
        LABEL_REGISTER_SUB_TITLE: 'Registrar por meio de um e-mail',
        LABEL_AUTH_SOCIAL: 'Autenticar por meio das redes sociais',
        LABEL_TEACHER: 'Professor Responsável',
        LABEL_EXIT: 'Sair',
        LABEL_INIT: 'Início',
        LABEL_EDIT: 'Editar',
        LABEL_DEL: 'Excluir',
        LABEL_DEFAULT_TEAM: 'Equipe ',
        LABEL_TEAM_VALIDATED: 'Perguntas validas',
        LABEL_QUESTION_VALIDATED: 'Pergunta válida',
        LABEL_TEAM_CONFIGURE: 'Configurar',
        LABEL_TEAM_VALIDATING: 'Validando perguntas',
        LABEL_QUESTION_VALIDATING: 'Validando pergunta',
        LABEL_TEAM_NO_VALIDATE: 'Perguntas não validadas',
        LABEL_QUESTION_NO_VALIDATE: 'Pergunta não válida',
        LABEL_TEAM_ACCESS_KEY: 'Chave de acesso da equipe: ',
        LABEL_TEAM_LIMIT_REACHED: 'Você atingiu o limite de cadastro de equipes!',
        LABEL_DELETE_EVENT_QUESTION: 'Todos os dados serão perdidos, deseja realmente excluir essa gincana?',
        LABEL_DELETE_TEAM_QUESTION: 'Todos os dados serão perdidos, deseja realmente excluir essa equipe?',
        LABEL_NOTICE: 'Esta equipe ainda deve cadastrar ',
        LABEL_EMPTY_STUDENT: 'Por favor, informe um aluno que não esteja cadastrado ou delete este campo!',
        LABEL_STUDENT_NAME: 'Nome do aluno',
        LABEL_ADD_STUDENT: 'Adicionar Aluno',
        LABEL_STUDENT: 'Aluno',

        LABEL_QUESTION: 'Enunciado',
        LABEL_SITUATION: 'Situação',
        LABEL_ALTERNATIVES: 'Alternativa',
        LABEL_CORRECT_ALTERNATIVE: 'Marque a alternativa correta',

        LABEL_TITLE_FORM_QUESTION: 'Cadastro de perguntas',

        LABEL_EVENT_NAME_EXIST: 'O nome informado já está em uso por outra gincana',
        LABEL_TEAM_NAME_EXIST: 'O nome informado já está em uso por outra equipe',
        LABEL_TEAM_AREA: 'Área da equipe',

        LABEL_ADD_EVENT: 'Nova Gincana',
        LABEL_MANAGE_TEAM: 'Gerênciar Equipes',
        LABEL_FORM_TEAM_ACCESS: 'Insira a chave de acesso da equipe',
        LABEL_ACCESS: 'Acessar',
        LABEL_INVALID_ACCESS_KEY: 'Chave de acesso inválida',

        LABEL_TITLE_MODAL_SEND_MAIL: 'E-mail enviado',
        LABEL_CONTENT_MODAL_SEND_MAIL: <span>E-mail de verificação enviado,<br />verifique sua caixa de span caso não tenha recebido o e-mail!</span>,
        LABEL_OK_TEXT_MODAL_SEND_MAIL: 'Prosseguir para Login',

        LABEL_CONTENT_MODAL_RECOVERY_MAIL: <span>Email de recuperação de senha enviado,<br />verifique sua caixa de span caso não tenha recebido o e-mail!</span>,

        LABEL_TITLE_ERROR_MODAL: 'Houve um erro',

        LABEL_EMAIL: 'E-mail',
        LABEL_NAME: 'Nome',
        LABEL_DATA_REALIZACAO: 'Data Realização',
        LABEL_PASSWORD: 'Senha',
        LABEL_CONFIRM_PASSWORD: 'Confirme a senha',
        LABEL_INIT_REGISTER: 'Iniciar cadastro',
        LABEL_PASSWORDS_DO_NOT_MATCH: 'As senhas não coincidem',
        LABEL_FINISHED: 'Concluir',
        LABEL_THEME: 'Tema',
        LABEL_EVENT: 'Gincana',
        LABEL_EVENT_MANAGER: 'Gerenciar Gincana',
        LABEL_ADD_TEAM: 'Adicionar Equipe',
        LABEL_ADD_TEAM_ACESS: 'Deseja criar acesso a uma nova equipe?',
        LABEL_ADD_TEAM_ACESS_NUMBER: 'Número de acessos disponíveis: ',

        LABEL_QUESTION_VALID: 'Esta pergunta é válida?',
        LABEL_YES: 'Sim',
        LABEL_NO: 'Não',

        LABEL_NOTE: 'Observações',
        LABEL_ADD_NOTE: 'Insira uma observação',

        LABEL_ADD_QUESTION: 'Adicionar Pergunta',
        LABEL_SEND_QUESTION: 'Enviar Pergunta',
        LABEL_TITLE_CONFIRM_REGISTER_MODAL: 'Confirmar o registro do usuário?',
        LABEL_REGISTER: 'Registrar',

        LABEL_INVALID_MAIL: 'E-mail inválido ou não informado!',
        LABEL_CANT_SEND_MAIL: 'Não foi possível reenviar o e-mail, tente novamente mais tarde',

        LABEL_REQUIRED: 'Campo obrigatório!',
        LABEL_PLEASE_INSERT_MAIL: 'Por favor, insira seu e-mail',
        LABEL_PLEASE_INSERT_PASSWORD: 'Por favor, insira sua senha!',
        LABEL_PLEASE_INSERT_PASSWORD_AGAIN: 'Por favor, insira novamente a senha!',
        LABEL_INFORMED_EMAIL_IS_ALREADY_BEING_USED: 'O e-mail informado já está sendo usado no sistema.',

        LABEL_TIME: 'Tempo de resposta em segundos',
        LABEL_PLEASE_INSERT_NAME: 'Por favor insira seu nome.',
        LABEL_PLEASE_INSERT_TEAM_NAME: 'Por favor insira o nome da equipe.',
        LABEL_PLEASE_INSERT_TEAM_WARCRY: 'Por favor insira o grito de guerra da equipe.',
        LABEL_PLEASE_INSERT_THEME: 'Por favor insira um tema.',
        LABEL_PLEASE_INSERT_DATE: 'Por favor insira uma data.',
        LABEL_PLEASE_INSERT_TIME: 'Por favor insira um tempo entre 5 a 300 segundos.',

        LABEL_PLEASE_INSERT_QUESTION: 'Por favor insira o enunciado.',
        LABEL_PLEASE_INSERT_ALTERNATIVE: 'Por favor insira uma alternativa.',
        LABEL_QUESTION_TIME_ESPIRED: 'O prazo para cadastro expirou',

        LABEL_REGISTER: 'Registro',
        LABEL_PASSWORD_RECOVER: 'Recuperar senha',

        LABEL_TITLE_MODAL_VERIFY_ACCOUNT: 'Ooops, Conta bloqueada!',
        LABEL_CONTENT_MODAL_VERIFY_ACCOUNT: 'Por favor, valide sua conta por meio do e-mail enviado.',
        LABEL_OK_TEXT_MODAL_VERIFY_ACCOUNT: 'Reenviar e-mail',
        LABEL_BACK_TEXT: 'Voltar',

        LABEL_ERROR_TITLE_MODAL: 'Houve um erro',

        LABEL_TITLE_MODAL_PASSWORD_RECOVERY: 'Informe o e-mail para recuperação',
        LABEL_ENTER: 'Entrar',
        LABEL_PASSWORD_RECOVERY: 'Recuperar Senha',

        LABEL_CONT_CAD_INFO: 'Informe os dados restantes para o cadastro no sistema',

        LABEL_LOADING: 'Aguarde estamos recuperando algumas informações',

        LABEL_NEW: 'Novo',
        LABEL_SAVE: 'Salvar',
        LABEL_ICON: 'Ícone',
      }
    };
  }

}

export default new Constants().constants;