import React, { Component } from 'react';
import { Router, Route, Redirect } from 'react-router';
import createBrowserHistory from 'history/createBrowserHistory';
import Register from 'Pages/Register/Register';
import EventPage from 'Pages/Event/EventPage';
import Login from 'Pages/Auth/Login';
import TeamAuth from 'Pages/Auth/TeamAuth';
import Init from 'Pages/Auth/Init';
import NoSupportBrowser from 'Pages/Auth/NoSupportBrowser';
import TeamManagePage from 'Pages/Team/TeamManagePage';
import TeamFormPage from 'Pages/Team/TeamFormPage';
import HomePage from 'Pages/Home/HomePage';
import CentralCommand from './Controllers/CentralCommand';
import Store from './Util/Store';
import Constants from './Util/Constants';
import '../public/css/app.css';
import '../public/css/router.css';
import 'babel-polyfill';
import { Row, Col, Spin } from 'antd';

/**
 * Classe resposável por definir todas as rotas do sistema
 */
export default class App extends Component {

  /**
	 * Construtor da classe
	 */
  constructor() {
    super();
    this.state = {
      centralCommand: CentralCommand.getInstance(),
      store: Store.getInstance(),
      user: 'empty',
      isTeacher: false
    }
  }

  componentDidMount = () => {
    let firebase = this.state.store.getFirebaseInstance();
    firebase.auth().onAuthStateChanged((firebaseUser) => {
      if (this.state.user == 'empty') {
        this.setState({
          user: firebaseUser,
          isTeacher: firebaseUser != null ? firebaseUser.email != Store.getInstance().TEAM_ACCESS_USER : false
        });
      }
    });
  }


  /**
   * É por meio deste render que renderizamos as rotas no index.js
   */
  render() {
    //No momento temos apenas linguagem em PT-BR
    // Internet Explorer 6-11
    let isIE = /*@cc_on!@*/false || !!document.documentMode;

    // Edge 20+
    let isEdge = !isIE && !!window.StyleMedia;

    return (
      isIE || isEdge ? <NoSupportBrowser labels={Constants[this.state.store.lang]} /> :
        this.state.user == 'empty' ?
          <Row className="routerRowFlex">
            <Spin spinning={true} size="large" ></Spin>
          </Row> :
          <Router history={createBrowserHistory({ forceRefresh: true })} >
            <div>
              <Route exact path='/' render={() => <Init centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />} />
              <Route exact path='/register' render={() => <Register centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />} />
              <Route exact path='/init' render={() => <Init centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />} />
              <Route exact path='/teams' render={() => <TeamAuth centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />} />
              <Route exact path='/teamform' render={() => (
                this.state.user ?
                  <TeamFormPage centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} /> :
                  <Redirect to='/teams' />
              )} />
              <Route exact path='/login' render={() => (
                this.state.user && this.state.isTeacher && this.state.user.emailVerified ?
                  <Redirect to='/home' /> :
                  <Login centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />
              )} />
              <Route exact path='/newEvent' render={() => (
                this.state.user && this.state.isTeacher ?
                  <EventPage centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} /> :
                  <Redirect to='/init' />
              )} />
              <Route exact path='/home' render={() => (
                this.state.user && this.state.isTeacher ?
                  <HomePage centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />
                  : <Redirect to='/init' />
              )} />
              <Route exact path='/team' render={() => (
                this.state.user && this.state.isTeacher ?
                  <TeamManagePage centralCommand={this.state.centralCommand} labels={Constants[this.state.store.lang]} store={this.state.store} />
                  : <Redirect to='/init' />
              )} />
            </div>
          </Router>
    );
  }
};