import '../../../public/css/common.css';

import React, { Component } from 'react';
import { Menu, Layout, Row, Col, Spin, Avatar, Tooltip, notification } from 'antd';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';
const { Header, Footer, Content, Sider } = Layout;
import { LocaleProvider } from 'antd';
import pt_BR from 'antd/lib/locale-provider/pt_BR';

import bookImage from '../../../public/images/book-8-bits.png';
import accountIcon from '../../../public/icons/user-6.svg';
import homeIcon from '../../../public/icons/home-2.svg';
import teamIcon from '../../../public/icons/teamwork-1.svg';
import eventIcon from '../../../public/icons/megaphone.svg';
import downloadIcon from '../../../public/icons/download.svg';
import exitIcon from '../../../public/icons/exit-1.svg';
import pack from '../../../package.json';

/**
 * Componente Responsável por prover o molde de sustentação dos menus no sistema.
 * 
 */

@observer
@autobind
export default class CommonPage extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      collapsed: false,
      mode: 'inline',
      loading: false,
      deathStar: DeathStar.getInstance(React),
      initialBar: false
    }
  }


  /**
   * Executado sempre que o componente termina sua montagem
   */
  componentDidMount() {
    window.onpopstate = () => {
      window.location.reload();
    }

    this.createSiderPageLayout();
    this.createSideContentLinks();
    this.processPage();
    this.setState({
      collapsed: false
    });

    setTimeout(() => { this.setState({ collapsed: true, initialBar: true }) }, 2000);
  }


  /**
   * Executado sempre que o componente acabar de ser atualizado.
   * 
   * @param {object} nextProps 
   * @param {object} nextState 
   */
  componentWillUpdate(nextProps, nextState) {
    if (this.state.deathStar.getElement('Sider')) this.state.deathStar.manipulate('Sider').modifyAttribute({ collapsed: nextState.collapsed });
    if (this.state.deathStar.getElement('Sider-Menu1-MenuItem3')) {
      this.state.deathStar.manipulate('Sider-Menu1-MenuItem3').modifyAttribute({ style: { display: this.props.store.isValidEvent ? 'block' : 'none' } })
    }
  }

  /**
   * Executado sempre que o componente termina de ser atualizado.
   * 
   * @param {object} prevProps 
   * @param {object} prevState 
   */
  componentDidUpdate(prevProps, prevState) {
    this.notifyError(prevProps.store.error);
  }

  /**
   * Expande e contrai a barra lateral de opções
   * 
   * @param {boolean} collapsed 
   */
  onCollapse(collapsed) {
    this.setState({
      collapsed,
      mode: collapsed ? 'vertical' : 'inline',
    });
  }

  /**
   * Retorna o componente de load das informações
   */
  getLoadingPage() {
    return (
      <Spin spinning={this.state.loading} tip={<span className='fontLoading'>{this.props.labels.LABEL_LOADING}</span>} size="large" >
        <div className="container backgroundLoad" />
      </Spin>
    );
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  toHome() {
    window.history.pushState(null, "", '/home');
    window.location.reload();
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  logout() {
    this.props.centralCommand.executeAction('./Auth', 'logout');
  }

  /**
   * Cria os links de navegação do sistema que vai dentro do SideBar.
   */
  createSideContentLinks() {
    this.state.deathStar.manipulate('Sider').setChildren(
      <Menu theme="dark" mode={this.state.mode} className='menuBlock'>
        <Menu.Item key="1">
          <span className="menuItem" onClick={this.toHome}>
            <img src={homeIcon} style={{ width: 30, height: 30 }} alt="icon" />
            <span className="labelMenu">{this.props.labels.LABEL_HOME}</span>
          </span>
        </Menu.Item>
      </Menu>, 1);
  }

  /**
   * Encapsula o documento em uma tag e inicia o download.
   */
  createAJsonExportFile() {
    const locationState = window.history.state;
    const dataAtualizacao = locationState ? locationState.gincanaAtualizacao : '';

    this.props.centralCommand.executeAction('./Event', 'processFileJson', [dataAtualizacao.key, (jsonObj) => {
      var blob = new Blob([JSON.stringify(jsonObj.value)], { type: 'application/json' });
      var url = URL.createObjectURL(blob);
      var blobAnchor = document.createElement('a');
      blobAnchor.download = jsonObj.name;
      blobAnchor.href = url;
      document.body.appendChild(blobAnchor); // required for firefox

      blobAnchor.onclick = function () {
        requestAnimationFrame(function () {
          URL.revokeObjectURL(url);
        })
      };

      blobAnchor.click();
    }]);
  }

  /**
   * Exibe o erro do sistema com a mensagem especifica do problema.
   * 
   * @param {object} error 
   */
  notifyError(error) {
    if (error) {
      notification['error']({
        message: this.props.labels.LABEL_ERROR_TITLE_MODAL,
        description: error,
        onClose: () => { this.props.store.error = '' }
      });
    }
  };

  /**
   * Cria o layout do Sidebar e o local para inserção da página dinâmica
   */
  createSiderPageLayout() {
    this.state.deathStar.processElement(
      <Sider
        collapsible
        collapsed={this.state.collapsed}
        onCollapse={this.onCollapse}
        width={300}
        className="siderColor"
      />
      , 'Sider');

    this.state.deathStar.processElement(
      <Header className='commonHeader'>
        <Row type='flex' justify='start'>
          <Col><span className="fontKarmaFuture logoText">{this.props.labels.LABEL_GINCO}</span><img src={bookImage} className="imgLogo" alt="logo" /></Col>
        </Row>
        <Row className="exit">
          <span className="nameAccount">{this.props.store.isTeacher ? this.props.store.user.displayName ? this.props.store.user.displayName : this.props.store.user.email : ""}</span>
          <Tooltip placement="bottom" title={this.props.labels.LABEL_EXIT}>
            <Avatar onClick={this.logout} shape="square" size="large" src={exitIcon} />
          </Tooltip>
        </Row>
      </Header>
      , 'Header');
  }

  /**
   * Método responsável por abrir a rota de times da gincana
   */
  toTeam() {
    window.history.pushState({ gincana: this.props.store.gincanaElementData ? this.props.store.gincanaElementData : window.history.state.gincanaAtualizacao }, "", '/team');
    window.location.reload();
  }

  /**
   * Método responsável por abrir a rota de gerencia da gincana
   */
  toEvent() {
    window.history.pushState({ gincanaAtualizacao: this.props.store.gincanaElementData ? this.props.store.gincanaElementData : window.history.state.gincanaAtualizacao }, "", '/newEvent');
    window.location.reload();
  }

  /**
   * Realiza o processamento da página de acordo com a rota ativa.
   */
  processPage() {
    switch (window.location.pathname) {
      case '/home':
        this.state.deathStar.manipulate('Sider-Menu1').removeChildren(1);
        this.state.collapsed = true;
        break;
      case '/newEvent':
        let locationState = window.history.state;
        if (this.props.store.gincanaElement || locationState && locationState.gincanaAtualizacao) {
          this.state.deathStar.manipulate('Sider-Menu1').setChildren(
            <Menu.Item key="2">
              <span className="menuItem" onClick={this.toTeam}>
                <img src={teamIcon} style={{ width: 30, height: 30 }} alt="icon" />
                <span className="labelMenu">{this.props.labels.LABEL_TEAM_MANAGER}</span>
              </span>
            </Menu.Item>);
        }

        this.state.deathStar.manipulate('Sider-Menu1').setChildren(
          <Menu.Item key="3" style={{ display: this.props.store.isValidEvent ? 'block' : 'none' }}>
            <span className="menuItem" onClick={this.createAJsonExportFile}>
              <img src={downloadIcon} style={{ width: 30, height: 30 }} alt="icon" />
              <span className="labelMenu">{this.props.labels.LABEL_EXPORT_DATA}</span>
            </span>
          </Menu.Item>);

        this.state.collapsed = true;
        break;
      case '/teamform':
        if (!this.props.store.isTeacher) {
          this.state.deathStar.destroy('Sider');
        } else {
          this.state.deathStar.manipulate('Sider-Menu1').setChildren([
            <Menu.Item key="3">
              <span className="menuItem" onClick={this.toEvent}>
                <img src={eventIcon} style={{ width: 30, height: 30 }} alt="icon" />
                <span className="labelMenu">{this.props.labels.LABEL_EVENT_MANAGER}</span>
              </span>
            </Menu.Item>,
            <Menu.Item key="2">
              <span className="menuItem" onClick={this.toTeam}>
                <img src={teamIcon} style={{ width: 30, height: 30 }} alt="icon" />
                <span className="labelMenu">{this.props.labels.LABEL_TEAM_MANAGER}</span>
              </span>
            </Menu.Item>]);
        }
        break;
      case '/team':
        this.state.deathStar.manipulate('Sider-Menu1').setChildren([
          <Menu.Item key="3">
            <span className="menuItem" onClick={this.toEvent}>
              <img src={eventIcon} style={{ width: 30, height: 30 }} alt="icon" />
              <span className="labelMenu">{this.props.labels.LABEL_EVENT_MANAGER}</span>
            </span>
          </Menu.Item>]);
        break;
    }
  }

  /**
   * Renderiza o componente
   */
  render() {
    //variveis observadas da store
    this.props.store.user;
    this.props.store.error;
    this.props.store.gincanaElement;
    this.props.store.isValidEvent;

    return (
      <div>
        <LocaleProvider locale={pt_BR}>
          <Spin spinning={this.props.store.loading || this.props.store.loadingUpdates} size="large" >
            <Layout className="container">
              {this.state.deathStar.getElement('Sider')}
              <Layout className='common'>
                {this.state.deathStar.getElement('Header')}
                <Content style={{ margin: '0 16px' }}>
                  {this.props.children}
                </Content>
              </Layout>
              <Footer className='footerCad' style={{ fontFamily: 'Lato Light', fontSize: '1.4em' }}>
                <span className="fontKarmaFuture" >{this.props.labels.LABEL_FOOTER} {pack.version}</span>
              </Footer>
            </Layout>
          </Spin>
        </LocaleProvider>
      </div>
    );
  }
}