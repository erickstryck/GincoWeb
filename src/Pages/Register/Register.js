import React, { Component } from 'react';
import { Row, Col, Layout, Card, Form, Modal, notification, Spin, Avatar } from 'antd';
import '../../../public/css/register.css';
import FormRegister from './FormRegister';
import autobind from 'autobind-decorator';
import { observer } from 'mobx-react';

const { Footer, Content } = Layout;

const FormRegisterComponent = Form.create()(FormRegister);

import bookImage from '../../../public/images/book-8-bits.png';
import gplusIcon from '../../../public/icons/033-google-plus.svg';
import facebookIcon from '../../../public/icons/036-facebook.svg';

/**
 * Componente Responsável por realizar o cadastro inicial do usuário
 * 
 */

@observer
@autobind
export default class Register extends Component {

	/**
	 * Construtor da classe
	 */
  constructor() {
    super();
    this.state = {
      modalInfo: false
    }
  }

	/**
	 * Executado sempre que o componente acabar de ser atualizado.
	 * 
	 * @param {object} prevProps 
	 * @param {object} prevState 
	 */
  componentDidUpdate(prevProps, prevState) {
    this.notifyError(prevProps.store.error);
    this.modalInfo(prevProps.store.initReg);
  }

	/**
	 * Exibe o erro do sistema com a mensagem especifica do problema.
	 * 
	 * @param {object} error 
	 */
  notifyError(error) {
    if (error) {
      notification['error']({
        message: this.props.labels.LABEL_ERROR_TITLE_MODAL,
        description: error,
        onClose: () => { this.props.store.error = '' }
      });
    }
  };

  /**
   * Método responsável por realizar a ação de renvio de email
   */
  toLogin() {
    window.history.pushState(null,"",'/login');
    window.location.reload();
  }

	/**
	 * Método responsável por abrir o modal de informação do e-mail enviado
	 * 
	 * @param {object} storeData 
	 */
  modalInfo(initReg) {
    if (initReg && !this.state.modalInfo) {
      this.state.modalInfo = !this.state.modalInfo;

      Modal.confirm({
        title: this.props.labels.LABEL_TITLE_MODAL_SEND_MAIL,
        content: this.props.labels.LABEL_CONTENT_MODAL_SEND_MAIL,
        okText: this.props.labels.LABEL_OK_TEXT_MODAL_SEND_MAIL,
        cancelText: this.props.labels.LABEL_BACK_TEXT,
        onOk: () => {
          this.props.store.initReg = '';
          this.toLogin();
        },
        onCancel: () => {
          this.state.modalInfo = !this.state.modalInfo;
          this.props.store.initReg = '';
        }
      });
    }
  }

	/**
	 * Renderiza o componente
	 */
  render() {

    //variveis observadas da store
    this.props.store.error;
    this.props.store.user;
    this.props.store.initReg;
    this.props.store.loading;

    return (
      <Layout className='cad' >
        <Modal />
        <Content className='contentCad'>
          <Row type='flex' justify='center' className="cardCenter">
            <Col xl={9} lg={12} md={24} sm={32}>
              <Card>
                <Row type='flex' justify='center'>
                  <Col><span className="fontKarmaFuture logoText">{this.props.labels.LABEL_GINCO}</span><img src={bookImage} className="imgLogo" alt="logo" /></Col>
                </Row>
                <Row>
                  <Col><span className="title">{this.props.labels.LABEL_LOGIN_TITLE}</span></Col>
                </Row>
                <Row className="subTitleRow">
                  <Col><span className="subTitle">{this.props.labels.LABEL_REGISTER_SUB_TITLE}</span></Col>
                </Row>
                <Spin spinning={this.props.store.loading} size="large" >
                  <Row className='form'>
                    <Col span={24}>
                      <FormRegisterComponent {...this.props} modalInfo={this.modalInfo} />
                    </Col>
                  </Row>
                </Spin>
              </Card>
              <Row>
                <Col span={1} offset={22}>
                  <span className='link' onClick={this.toLogin}>{this.props.labels.LABEL_BACK_TEXT}</span>
                </Col>
              </Row>
            </Col>
          </Row>
        </Content>
        <Footer className='footerCad' style={{ fontFamily: 'Lato Light', fontSize: '1.4em' }}>
          <span className="fontKarmaFuture" >{this.props.labels.LABEL_FOOTER}</span>
        </Footer>
      </Layout>
    );
  }
}