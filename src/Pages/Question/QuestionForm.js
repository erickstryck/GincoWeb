import React, { Component } from 'react';
import { Form, Input, Button, Row, Radio, Col, Avatar } from 'antd';
import '../../../public/css/register.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';

import noteImg from '../../../public/icons/note.svg';

const RadioGroup = Radio.Group;

const FormItem = Form.Item;

const { TextArea } = Input;

/**
 * Componente Responsável por criar o formulário de cadastro
 * 
 */
@observer
@autobind
class QuestionForm extends Component {

	/**
	 * Construtor da classe
	 */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React),
      confirmDirty: false,
      correctAlternative: 1,
      updateKey: '',
      hasInit: false,
      isValid: true
    };
  }

	/**
	 * Realiza as ações quando se monta o componente
	 */
  componentDidMount() {
    this.props.centralCommand.executeAction('./Question', 'getRulesOfQuestion', [this.checkConfirm]);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.data && !state.hasInit) {
      state.isValid = typeof props.data.validate === "boolean" ? props.data.validate : true;
      for (let i = 0; i <= 3; i++) {
        if (props.data.alternativas[i].isCorreta) {
          state.correctAlternative = ++i;
          state.updateKey = props.data.key;
          break;
        }
      }
    } else if (!props.data) {
      state.updateKey = '';
    }

    state.hasInit = props.init;

    return state;
  }

	/**
	 * Método responsável por verficar se os campos informados estão corretamente preenchidos.
	 * 
	 * @param {object} rule 
	 * @param {object} value 
	 * @param {object} callback 
	 */
  checkConfirm(rule, value, callback) {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

	/**
	 *  Verifica se o campo é válido ao tirar o foco do mesmo e grava isso em um estado.
	 * 
	 * @param {event} e 
	 */
  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

	/**
	 * Processo os dados do formulário para envio
	 * 
	 * @param {event} e 
	 */
  handleForm(e) {
    e.preventDefault();

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.handleModal();
        this.props.form.resetFields();
        if (this.state.updateKey) {
          this.props.centralCommand.executeAction('./Question', 'update', [values, this.state.correctAlternative, this.state.isValid, this.state.updateKey]);
        } else {
          this.props.centralCommand.executeAction('./Question', 'save', [values, this.state.correctAlternative]);
        }
        this.state.correctAlternative = 1;
      }
    });
  }

	/**
	 * Manipula as ecolhas da alternativa verdadeira.
	 * 
	 * @param {Object} element 
	 */
  handleAlternatives(element) {
    this.setState({
      correctAlternative: element.target.value,
    });
  }

  /**
	 * Manipula as ecolhas da validação.
	 * 
	 * @param {Object} element 
	 */
  handleValidate(element) {
    this.setState({
      isValid: element.target.value
    })
  }

	/**
	 * Renderiza o componente
	 */
  render() {
    let { getFieldDecorator } = this.props.form;
    const fontStyle = { fontFamily: 'Lato Regular', fontSize: '1.4em' };

    const radioStyle = {
      display: 'block',
      height: '30px',
      lineHeight: '30px',
    };

    return (
      <Form onSubmit={this.handleForm}>
        <Row type="flex" justify="center" className="questionForm">
          <Row style={{ width: "70%" }}>
            <FormItem
              label={<span style={fontStyle}>{this.props.labels.LABEL_QUESTION}</span>}
              hasFeedback
            >
              {this.props.store.dataQuestionRules.question ? getFieldDecorator('enunciado', { rules: this.props.store.dataQuestionRules.question, initialValue: this.props.data ? this.props.data.enunciado : '' })(<TextArea rows={3} />) : ''}
            </FormItem>
            <FormItem
              label={<span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(A)"}</span>}
              hasFeedback>
              {this.props.store.dataQuestionRules.alternative ? getFieldDecorator('alternativa_1', { rules: this.props.store.dataQuestionRules.alternative, initialValue: this.props.data ? this.props.data.alternativas[0].descricao : '' })(<TextArea rows={2} />) : ''}
            </FormItem>
            <FormItem
              label={<span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(B)"}</span>}
              hasFeedback>
              {this.props.store.dataQuestionRules.alternative ? getFieldDecorator('alternativa_2', { rules: this.props.store.dataQuestionRules.alternative, initialValue: this.props.data ? this.props.data.alternativas[1].descricao : '' })(<TextArea rows={2} />) : ''}
            </FormItem>
            <FormItem
              label={<span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(C)"}</span>}
              hasFeedback>
              {this.props.store.dataQuestionRules.alternative ? getFieldDecorator('alternativa_3', { rules: this.props.store.dataQuestionRules.alternative, initialValue: this.props.data ? this.props.data.alternativas[2].descricao : '' })(<TextArea rows={2} />) : ''}
            </FormItem>
            <FormItem
              label={<span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(D)"}</span>}
              style={{ padding: 'none !important' }}
              hasFeedback>
              {this.props.store.dataQuestionRules.alternative ? getFieldDecorator('alternativa_4', { rules: this.props.store.dataQuestionRules.alternative, initialValue: this.props.data ? this.props.data.alternativas[3].descricao : '' })(<TextArea rows={2} />) : ''}
            </FormItem>
            {this.props.data.note ?
              <Row>
                <p style={{ fontWeight: 'bold' }}><Avatar shape="square" src={noteImg} /> {this.props.data.note}</p>
              </Row> : <div />}
          </Row>
          <Row className="flexColumn questionColumn">
            <Col>
              <span style={fontStyle}>{this.props.labels.LABEL_CORRECT_ALTERNATIVE}</span>
            </Col>
            <Col>
              <RadioGroup name="isCorrect" onChange={this.handleAlternatives} value={this.state.correctAlternative}>
                <Radio style={radioStyle} value={1}><span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(A)"}</span></Radio>
                <Radio style={radioStyle} value={2}><span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(B)"}</span></Radio>
                <Radio style={radioStyle} value={3}><span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(C)"}</span></Radio>
                <Radio style={radioStyle} value={4}><span style={fontStyle}>{this.props.labels.LABEL_ALTERNATIVES + "(D)"}</span></Radio>
              </RadioGroup>
            </Col>
            {this.props.store.isTeacher && this.props.data ?
              <div style={{ marginTop: '38px' }}>
                <Col>
                  <span style={fontStyle}>{this.props.labels.LABEL_QUESTION_VALID}</span>
                </Col>
                <Col>
                  <RadioGroup name="validate" onChange={this.handleValidate} value={this.state.isValid}>
                    <Radio style={radioStyle} value={true}><span style={fontStyle}>{this.props.labels.LABEL_YES}</span></Radio>
                    <Radio style={radioStyle} value={false}><span style={fontStyle}>{this.props.labels.LABEL_NO}</span></Radio>
                  </RadioGroup>
                </Col>
                {!this.state.isValid ?
                  <FormItem
                    label={<span style={fontStyle}>{this.props.labels.LABEL_NOTE}</span>}
                    hasFeedback>
                    {this.props.store.dataQuestionRules.note ? getFieldDecorator('note', { rules: this.props.store.dataQuestionRules.note, initialValue: this.props.data ? this.props.data.note : '' })(<TextArea rows={6} />) : ''}
                  </FormItem> : <div />}
              </div> : <div />}
            <Col>
              <Button className='cadButton' style={{ fontFamily: 'Lato Regular', fontSize: '1.6em', marginTop: 40 }} htmlType="submit" size="large">{this.props.labels.LABEL_SEND_QUESTION}</Button>
            </Col>
          </Row>
        </Row>
      </Form>
    );
  }
}

export default QuestionForm;