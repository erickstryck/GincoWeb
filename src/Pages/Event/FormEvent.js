import React, { Component } from 'react';
import { Form, Input, Button, Row, DatePicker, InputNumber } from 'antd';
import '../../../public/css/eventPage.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import moment from 'moment';
import 'moment/locale/pt-br';

const FormItem = Form.Item;

/**
 * Componente Responsável por criar o formulário de cadastro
 * 
 */
@observer
@autobind
export default class FormEvent extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      confirmDirty: false,
      eventKey: ''
    };
  }

  /**
   * Realiza as ações quando se monta o componente
   */
  componentDidMount() {
    this.props.centralCommand.executeAction('./Event', 'isValidEvent', [this.state.eventKey]);
    this.props.centralCommand.executeAction('./Event', 'getRulesOfNewEvent', [this.checkConfirm]);
  }

  /**
   * Método responsável por verficar se os campos informados estão corretamente preenchidos.
   * 
   * @param {object} rule 
   * @param {object} value 
   * @param {object} callback 
   */
  checkConfirm(rule, value, callback) {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  /**
   *  Verifica se o campo é válido ao tirar o foco do mesmo e grava isso em um estado.
   * 
   * @param {event} e 
   */
  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  /**
   * Processo os dados do formulário para envio
   * 
   * @param {event} e 
   */
  handleForm(e) {
    e.preventDefault();

    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        if (!values.key) {
          this.props.centralCommand.executeAction('./Event', 'saveEvent', [values]);
        } else {
          this.props.centralCommand.executeAction('./Event', 'updateEvent', [values]);
        }
      }
    });
  }

  /**
   * Valida se alguma gincana já possui o nome informado
   * 
   * @param {object} element 
   */
  verifyIfEventNameExist(element) {
    this.props.centralCommand.executeAction('./Event', 'verifyIfEventNameExist', [element.target.value]);
  }

  /**
   * Renderiza o componente
   */
  render() {
    const locationState = window.history.state;
    const dataAtualizacao = locationState ? locationState.gincanaAtualizacao : '';
    this.state.eventKey = dataAtualizacao.key;

    const { getFieldDecorator } = this.props.form;
    const fontStyle = { fontFamily: 'Lato Regular', fontSize: '1.4em' };

    let notifyStatus = this.props.store.notifyFieldValueExists;

    return (
      <Form onSubmit={this.handleForm}>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_NAME}</span>}
          validateStatus={notifyStatus.validateStatus}
          help={notifyStatus.help}
          hasFeedback
        >
          {this.props.store.dataRules.nameRules ? getFieldDecorator('nome', { rules: this.props.store.dataRules.nameRules, initialValue: dataAtualizacao.nome })(<Input onBlur={this.verifyIfEventNameExist} />) : ''}
        </FormItem>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_TEMA}</span>}
          hasFeedback>
          {this.props.store.dataRules.themeRules ? getFieldDecorator('tema', { rules: this.props.store.dataRules.themeRules, initialValue: dataAtualizacao.tema })(<Input />) : ''}
        </FormItem>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_TEACHER}</span>}
          hasFeedback>
          {this.props.store.dataRules.teacherRules ? getFieldDecorator('professor', { rules: this.props.store.dataRules.teacherRules, initialValue: dataAtualizacao.professor })(<Input />) : ''}
        </FormItem>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_TIME}</span>}
          hasFeedback>
          {this.props.store.dataRules.timeRules ? getFieldDecorator('time', { rules: this.props.store.dataRules.timeRules, initialValue: dataAtualizacao.time })(<InputNumber
            min={5} max={300} />) : ''}
        </FormItem>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_EVENT_DATE}</span>}
          hasFeedback>
          {this.props.store.dataRules.dataEventRules ? getFieldDecorator('dataRealizacao', { rules: this.props.store.dataRules.dataEventRules, initialValue: dataAtualizacao ? moment(dataAtualizacao.dataRealizacao, "DD-MM-YYYY") : undefined })(<DatePicker format="DD/MM/YYYY" size='default' />) : ''}
        </FormItem>
        <FormItem
          label={<span style={fontStyle}>{this.props.labels.LABEL_LIMIT_EVENT_DATE}</span>}
          hasFeedback>
          {this.props.store.dataRules.dataLimitEventRules ? getFieldDecorator('dataLimitePerguntas', { rules: this.props.store.dataRules.dataLimitEventRules, initialValue: dataAtualizacao ? moment(dataAtualizacao.dataLimitePerguntas, "DD-MM-YYYY") : undefined })(<DatePicker format="DD/MM/YYYY" size='default' />) : ''}
        </FormItem>
        <FormItem>
          {getFieldDecorator('key', { initialValue: dataAtualizacao.key })(<Input type="hidden" />)}
        </FormItem>
        <Row type="flex" justify="space-around" align="middle">
          <Button disabled={this.props.store.loadingUpdates} className='cadButton' style={{ fontFamily: 'Lato Regular', fontSize: '1.6em' }} htmlType="submit" size="large">{this.props.labels.LABEL_SAVE}</Button>
        </Row>
      </Form>
    );
  }
}