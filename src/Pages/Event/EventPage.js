import React, { Component } from 'react';
import { Row, Col, Layout, Form } from 'antd';
import autobind from 'autobind-decorator';
import CommonPage from '../Common/CommonPage';
import { observer } from "mobx-react";
import '../../../public/css/eventPage.css';
import FormEvent from './FormEvent';

const { Header, Content } = Layout;

const FormLoginComponent = Form.create()(FormEvent);

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */

@observer
@autobind
export default class EventPage extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
  }

  /**
    * Renderiza o componente
    */
  render() {
    return (
      <CommonPage {...this.props} {...this.state}>
        <Layout>
          <Header className='header'>
            <Row type='flex' justify='center'>
              <Col>
                <span className="homeTitle">{this.props.labels.LABEL_ADD_EVENT}</span>
              </Col>
            </Row>
          </Header>
          <Content>
            <Row type='flex' justify='center'>
              <Col xl={12} lg={12} md={12} sm={24}>
                <FormLoginComponent {...this.props} {...this.state} />
              </Col>
            </Row>
          </Content>
        </Layout>
      </CommonPage>
    );
  }
}
