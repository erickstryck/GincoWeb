import React, { Component } from 'react';
import { Row, Col, Layout, Popconfirm, Table, Button, Avatar, Tooltip } from 'antd';
import autobind from 'autobind-decorator';
import CommonPage from '../Common/CommonPage';
import DeathStar from 'react-deathStar';
import { observer } from "mobx-react";
import '../../../public/css/home.css';

const { Header, Content } = Layout;

import editIcon from '../../../public/icons/edit.svg';
import garbageIcon from '../../../public/icons/garbage-1.svg';

/**
 * Componente Responsável por mostrar as opções iniciais
 * 
 */
@observer
@autobind
export default class HomePage extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      dataSource: [],
      count: 0,
      deathStar: DeathStar.getInstance(React),
    }
  }

  /**
   * Executa as ações enquanto monta o componente
   */
  componentWillMount() {
    this.props.centralCommand.executeAction('./Event', 'getEvent');
    this.carregarColunasTabela();
  }

  /**
   * 
   * Carregas as informações das colunas apresentadas nas tabelas.
   */
  carregarColunasTabela() {
    this.columns = [{
      title: this.props.labels.LABEL_NAME,
      dataIndex: 'nome',
      align: 'center'
    }, {
      title: this.props.labels.LABEL_DATA_REALIZACAO,
      dataIndex: 'dataRealizacao',
      align: 'center'
    }, {
      title: this.props.labels.LABEL_TEMA,
      dataIndex: 'tema',
      align: 'center'
    }, {
      title: this.props.labels.LABEL_ACAO,
      dataIndex: 'operation',
      align: 'center',
      render: (text, record) => {
        return (
              <Row type="flex" justify="space-around" >
                <Col>
                  <Popconfirm title={this.props.labels.LABEL_DELETE_EVENT_QUESTION} onConfirm={() => this.onDelete(record.rowKey, record)}>
                    <Tooltip placement="top" title={this.props.labels.LABEL_DEL}>
                      <Avatar shape="square" size="small" src={garbageIcon} />
                    </Tooltip>
                  </Popconfirm>
                </Col>
                <Col>
                  <span onClick={() => this.updateEvent(record)}>
                    <Tooltip placement="top" title={this.props.labels.LABEL_EDIT}>
                      <Avatar shape="square" size="small" src={editIcon} />
                    </Tooltip>
                  </span>
                </Col>
              </Row>
        );
      },
    }];
  }

  /**
   * Atualiza a gincana por meio dos dados informados
   * 
   * @param {object} values 
   */
  updateEvent(values) {
    delete values.equipes;
    window.history.pushState({ gincanaAtualizacao: values },"",'/newEvent');
    window.location.reload();
  }

  /**
   * Executado quando o componente está atualizando
   * 
   * @param {object} nextProps 
   * @param {object} nextState 
   */
  componentWillUpdate(nextProps, nextState) {
    this.state.dataSource = nextProps.store.dataGincanas;
    this.state.count = nextProps.store.dataGincanas.length;
  }

  /**
   * Executa ações quando há mudanças no comportamento da tabela
   * 
   * @param {string} key 
   * @param {Object} dataIndex 
   */
  onCellChange(key, dataIndex) {
    return (value) => {
      const dataSource = [...this.state.dataSource];
      const target = dataSource.find(item => item.rowKey === key);
      if (target) {
        target[dataIndex] = value;
        this.setState({ dataSource });
      }
    };
  }

  /**
   * Executa a ação de remover gincanas a tabela
   */
  onDelete(key, record) {
    this.props.centralCommand.executeAction('./Event', 'deleteEvent', [record]);
    const dataSource = [...this.state.dataSource];
    this.setState({ dataSource: dataSource.filter(item => item.rowKey !== key) });
  }

  /**
   * Método responsável por abrir de cadastro de gincana
   */
  toNewEvent() {
    window.history.pushState(null,"",'/newEvent');
    window.location.reload();
  }

  /**
    * Renderiza o componente
    */
  render() {
    const columns = this.columns;
    //variveis observadas da store
    this.props.store.gincanas;

    return (
      <CommonPage {...this.props} {...this.state}>
        <Layout>
          <Header className='header'>
            <Row type='flex' justify='center'>
              <Col>
                <span className="homeTitle">{this.props.labels.LABEL_HOME_TITLE}</span>
              </Col>
            </Row>
          </Header>
          <Content>
            <Row type='flex' justify='end' >
              <Button className="buttonSearch" onClick={this.toNewEvent} size="large">{this.props.labels.LABEL_ADD_EVENT}</Button>
            </Row>
            <Row>
              <Table bordered dataSource={this.state.dataSource} columns={columns} pagination={{ defaultPageSize: 8 }} />
            </Row>
          </Content>
        </Layout>
      </CommonPage >
    );
  }
}
