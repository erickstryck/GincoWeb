import React, { Component } from 'react';
import { Row, Col, Layout, Button, Modal, Card, Tooltip, Avatar, Form, Upload, Icon, Divider } from 'antd';
import autobind from 'autobind-decorator';
import CommonPage from '../Common/CommonPage';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';
import TeamForm from './TeamForm';
import QuestionForm from '../Question/QuestionForm'
import '../../../public/css/teamPage.css';
import validatingImg from '../../../public/icons/validating-img.svg';
import validImg from '../../../public/icons/valid-img.svg';
import noValidateImg from '../../../public/icons/noValidate-img.svg';
import noteImg from '../../../public/icons/note.svg';
import teamOkImg from '../../../public/images/ok-team-img.png';

const { Header, Content } = Layout;

const FormTeamComponent = Form.create()(TeamForm);
const FormQuestionComponent = Form.create()(QuestionForm);


/**
 * Componente Responsável por prover o CRUD das equipes.
 */
@observer
@autobind
export default class TeamFormPage extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React),
      showModalCreateAccess: false,
      locationData: '',
      showModalQuestion: false,
      questionData: ''
    }
  }

  beforeUpload(file) {
    return this.props.centralCommand.executeAction('./Team', 'validateImage', [file]);
  }

  /**
   * Manipula o modal de cadastro de nova pergunta
   */
  handleModal() {
    this.setState({
      showModalQuestion: !this.state.showModalQuestion
    })
  }

  /**
   * Executado sempre que o componente acaba de ser atualizado.
   */
  componentDidMount() {
    this.props.store.gincanaElement = window.history.state.teamKeys.gincanaElement;
    this.props.centralCommand.executeAction('./Team', 'getTeamData', [this.state.locationData, this.createCardSlot]);
  }

  /**
   * Abre o modal de edição da pergunta
   * 
   * @param {object} questionData 
   */
  editQuestion(questionData) {
    this.state.questionData = questionData;
    this.handleModal();
  }

  /**
   * Abre o modal para adicionar uma pergunta
   * 
   * @param {object} questionData 
   */
  newQuestion() {
    this.state.questionData = '';
    this.handleModal();
  }

  createCardSlot() {
    this.state.deathStar.processElement(<Row type='flex' justify='center' />, "cardQuestionArea", false, this);
    let cardsQuestions = [];
    this.props.store.questions.forEach((question, index) => {
      if (question) {
        let states = [
          <Tooltip placement="top" title={this.props.labels.LABEL_QUESTION_VALIDATED}>
            <Row className="flexRow">
              <Avatar src={validImg} />
              {this.props.store.isTeacher ? <Icon type="setting" theme="outlined" style={{ fontSize: '2.3em', cursor: 'pointer' }} onClick={this.editQuestion.bind(this, question)} /> : <div />}
            </Row>
          </Tooltip>,
          <Tooltip placement="top" title={this.props.labels.LABEL_QUESTION_NO_VALIDATE}>
            <Row className="flexRow">
              <Avatar src={noValidateImg} />
              {this.props.store.expireQuestionCadPeriod && !this.props.store.isTeacher ? <div /> : <Icon type="setting" theme="outlined" style={{ fontSize: '2.3em', cursor: 'pointer' }} onClick={this.editQuestion.bind(this, question)} />}
            </Row>
          </Tooltip>,
          <Tooltip placement="top" title={this.props.labels.LABEL_QUESTION_VALIDATING}>
            <Row className="flexRow">
              <Avatar src={validatingImg} />
              {this.props.store.expireQuestionCadPeriod && !this.props.store.isTeacher ? <div /> : <Icon type="setting" theme="outlined" style={{ fontSize: '2.3em', cursor: 'pointer' }} onClick={this.editQuestion.bind(this, question)} />}
            </Row>
          </Tooltip>
        ];

        cardsQuestions.push(<Card
          title={<span style={{ textOverflow: 'ellipsis', overflow: 'hidden' }}>{++index}) {question.enunciado}</span>}
          extra={typeof question.validate === "boolean" ? question.validate ? states[0] : states[1] : states[2]}
          style={{ width: 600, margin: 50 }}
          className={typeof question.validate === "boolean" ? question.validate ? 'green' : 'red' : 'orange'}
        >
          {
            question.alternativas.map((alternativa, index) => {
              return <p key={this.props.store.getId()} style={{ color: alternativa.isCorreta ? '#00ef03' : 'red', fontWeight: 'bold' }} >{String.fromCharCode(97 + index).toUpperCase() + ") "}{alternativa.descricao}</p>
            })
          }
          {question.note ?
            <Row>
              <p style={{ fontWeight: 'bold' }}><Avatar shape="square" src={noteImg} /> {question.note}</p>
            </Row> : <div />}
        </Card>);
      }
    });

    for (let i = 1; i <= this.props.store.updatedQuestionSize; i++) {
      cardsQuestions.push(<Card
        title={this.props.labels.LABEL_EMPTY_QUESTION}
        style={{ width: 600, margin: 50 }}
      >
        <p>{this.props.labels.LABEL_EMPTY}</p>
      </Card>);
    }

    this.state.deathStar.manipulate("cardQuestionArea").setChildren(cardsQuestions, "", false, true);
  }

  /**
    * Renderiza o componente
    */
  render() {
    this.state.locationData = window.history.state.teamKeys;
    this.props.store.team;
    this.props.store.questions;
    this.props.store.updatedQuestionSize;

    let questionComponent = <FormQuestionComponent {...this.props} handleModal={this.handleModal} data={this.state.showModalQuestion ? this.state.questionData : ''} init={this.state.showModalQuestion} />;
    return (
      <CommonPage {...this.props} {...this.state}>
        <Layout>
          <Modal
            visible={this.state.showModalQuestion}
            title={this.props.labels.LABEL_TITLE_FORM_QUESTION}
            footer=""
            width="80vw"
            onCancel={this.handleModal}
          >
            {questionComponent}
          </Modal>
          <Header className='header'>
            <Row type='flex' justify='center'>
              <Col>
                <span className="homeTitle">{this.props.labels.LABEL_TEAM_AREA}</span>
              </Col>
            </Row>
          </Header>
          <Content>
            <Row className="flexRow">
              <Row className="flexColumn teamFormColumn">
                <Upload
                  name="avatar"
                  listType="picture-card"
                  className="avatar-uploader"
                  showUploadList={false}
                  action={(file) => {
                    this.props.centralCommand.executeAction('./Team', 'processImage', [file]);
                  }}
                  beforeUpload={this.beforeUpload}
                >
                  {this.props.store.team.image ? <img style={{
                    width: "100%"
                  }} src={this.props.store.team.image} /> : <Icon type="plus" />}
                </Upload>
                <span style={{ fontWeight: 'bold', fontSize: '1.5em', marginTop: 10 }}>{this.props.labels.LABEL_TEAM_ACCESS_KEY}</span>
                <span style={{ fontWeight: 'bold', fontSize: '1.5em' }}>{this.props.store.team.accessPass}</span>
              </Row>
              <Col style={{ width: "50%" }}>
                <FormTeamComponent {...this.props} />
              </Col>
            </Row>
            {!this.props.store.team.valid ? <Row className="flexRow">
              <span style={{ fontWeight: 'bold', color: 'red', fontSize: '2em' }}>{this.props.labels.MSG_NOTE_TEAM}</span>
            </Row> : <div />}
            {!this.props.store.team.valid ? (this.props.store.updatedQuestionSize > 0 || this.props.store.expireQuestionCadPeriod) ? <Divider><span style={{ fontWeight: 'bold', color: 'red', fontSize: '2em' }}>{this.props.store.expireQuestionCadPeriod ? this.props.labels.LABEL_QUESTION_TIME_ESPIRED : this.props.labels.LABEL_NOTICE + this.props.store.updatedQuestionSize + " " + this.props.labels.LABEL_QUESTIONS}</span></Divider> : <div /> : <div />}
            {this.props.store.team.valid ? <Divider><span style={{ fontWeight: 'bold', color: 'green', fontSize: '2em' }}>{this.props.labels.LABEL_VALID_TEAM}</span>  <Avatar style={{ width: "10vw", height: "10vw" }} src={teamOkImg} /></Divider> : <div />}
            <Divider>{this.props.labels.LABEL_QUESTIONS}</Divider>
            <Row type='flex' justify='center'>
              {this.props.store.updatedQuestionSize > 0 && (!this.props.store.expireQuestionCadPeriod || this.props.store.isTeacher) ? <Button className="buttonSearch" size="large" style={{ fontFamily: 'Lato Regular', fontSize: '1.7em' }} onClick={this.newQuestion}>{this.props.labels.LABEL_ADD_QUESTION}</Button> : <div />}
            </Row>
            {this.state.deathStar.getElement("cardQuestionArea")}
          </Content>
        </Layout>
      </CommonPage>
    );
  }
}
