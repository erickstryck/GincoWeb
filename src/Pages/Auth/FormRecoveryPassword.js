import React, { Component } from 'react';
import { Col, Layout, Form, Input, Button, Row } from 'antd';
import '../../../public/css/register.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';
import CentralCommand from '../../Controllers/CentralCommand';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

/**
 * Componente Responsável por criar o formulário de recuperação de senha
 * 
 */
@observer
@autobind
class FormRecoveryPassword extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      confirmDirty: false,
      deathStar: DeathStar.getInstance(React),
      submit: false
    };
  }

  /**
   * Realiza as ações enquanto monta o componente
   */
  componentWillMount() {
    this.props.centralCommand.executeAction('./Auth', 'getRulesOfPasswordRecovery');
  }

  /**
   * Realiza as ações após a conclusão da montagem do componente
   */
  componentDidMount() {
    this.state.deathStar.putStore("modalFactoryContext", this, true);
  }

  /**
   * Realiza as ações ao desmontar o componente
   */
  componentWillUnmount() {
    this.props.centralCommand.executeAction('./Auth', 'getRulesOfLogin', [this.checkConfirm]);
    this.state.deathStar.destroy("modalFactoryContext");
  }

  /**
   * Realiza as ações ao ocorrer uma atualização do componente.
   * 
   * @param {object} prevProps 
   * @param {object} prevState 
   */
  componentDidUpdate(prevProps, prevState) {
    if (prevState.submit) {
      this.processSubmitForm();
    }
  }


  /**
   * Método responsável por verficar se os campos informados estão corretamente preenchidos.
   * 
   * @param {object} rule 
   * @param {object} value 
   * @param {object} callback 
   */
  checkConfirm(rule, value, callback) {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  /**
   *  Verifica se o campo é válido ao tirar o foco do mesmo e grava isso em um estado.
   * 
   * @param {event} e 
   */
  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  /**
   * Dispara a ação de submit do formulário para que possa executar a recuperação de senha.
   */
  processSubmitForm() {
    this.state.submit = false;
    this.props.form.validateFields(function (error, value) {
      if (!error) {
        CentralCommand.getInstance().executeAction('./Auth', 'resetPassword', [value.email]);
        let loginContext = DeathStar.getInstance().getStore("loginContext");
        loginContext.state.modalPasswordRecovery = false;
        loginContext.update();
      }
    });
  }

  /**
   * Renderiza o componente
   */
  render() {
    const { getFieldDecorator } = this.props.form;
    const fontStyle = { fontFamily: 'Lato Regular', fontSize: '1.4em' };

    return (
      <Form>
        <FormItem
          {...formItemLayout}
          label={<span style={fontStyle}>{this.props.labels.LABEL_EMAIL}</span>}
          hasFeedback
        >
          {this.props.store.dataRules ? getFieldDecorator('email', { rules: this.props.store.dataRules.emailRules })(<Input />) : ''}
        </FormItem>
      </Form>
    );
  }
}

export default FormRecoveryPassword;