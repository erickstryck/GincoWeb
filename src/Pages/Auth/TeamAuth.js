import React, { Component } from 'react';
import { Row, Col, Layout, Card, Form, Modal, notification, Spin, Avatar, Input, Button } from 'antd';
import '../../../public/css/login.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";

const { Footer, Content } = Layout;

import bookImage from '../../../public/images/book-8-bits.png';
import lockedIcon from '../../../public/icons/locked-2.svg';

/**
 * Componente Responsável por realizar o cadastro inicial do usuário
 * 
 */
@observer
@autobind
class TeamAuth extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      modalVerifyAccount: false,
      modalPasswordRecovery: false,
      loading: false
    }
  }

  /**
   * Executado sempre que o componente acabar de ser atualizado.
   * 
   * @param {object} prevProps 
   * @param {object} prevState 
   */
  componentDidUpdate(prevProps, prevState) {
    this.notifyError(prevProps.store.error);
  }


  /**
   * Exibe o erro do sistema com a mensagem especifica do problema.
   * 
   * @param {object} error 
   */
  notifyError(error) {
    if (error) {
      notification['error']({
        message: this.props.labels.LABEL_ERROR_TITLE_MODAL,
        description: error,
        onClose: () => { this.props.store.error = '' }
      });

      this.state.loading = false;
    }
  };

  /**
   * Procura a chave no repositótio e autêntica a equipe
   */
  authTeam() {
    this.props.centralCommand.executeAction('./Auth', 'authTeam', [this.refs.accesskey.input.value]);
    this.setState({ loading: true })
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  toTeamForm() {
    window.history.pushState(null,"",'/team');
    window.location.reload();
  }

  /**
   * Método responsável por abrir a rota de registro
   */
  toInit() {
    window.history.pushState(null,"",'/init');
    window.location.reload();
  }

  /**
   * Renderiza o componente
   */
  render() {
    //variveis observadas da store
    this.props.store.error;

    return (
      <Spin spinning={this.state.loading} size="large" >
        <Layout className='container' >
          <Content className='contentCad'>
            <Row type='flex' justify='center' className="cardCenter">
              <Col xl={9} lg={12} md={24} sm={32}>
                <Card>
                  <Row type='flex' justify='center'>
                    <Col><span className="fontKarmaFuture logoText">{this.props.labels.LABEL_GINCO}</span><img src={bookImage} className="imgLogo" alt="logo" /></Col>
                  </Row>
                  <Row>
                    <Col><span className="title">{this.props.labels.LABEL_LOGIN_TITLE}</span></Col>
                  </Row>
                  <Row className="teamAccessLabel">
                    <Col><span className="title">{this.props.labels.LABEL_FORM_TEAM_ACCESS}</span></Col>
                  </Row>
                  <Row className="teamAccessInput">
                    <Col>
                      <Avatar shape="square" size="large" src={lockedIcon} />
                    </Col>
                    <Col>
                      <Input ref="accesskey" />
                    </Col>
                  </Row>
                  <Row className="rowFlex btnAccess">
                    <Button className='cadButton' style={{ fontFamily: 'Lato Regular', fontSize: '1.6em' }} onClick={this.authTeam} size="large">{this.props.labels.LABEL_ACCESS}</Button>
                  </Row>
                </Card>
                <Row className="link-around">
                  <Col span={1} offset={22}>
                    <span className="link pointer" onClick={this.toInit}>{this.props.labels.LABEL_INIT}</span>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Content>
          <Footer className='footerCad' style={{ fontFamily: 'Lato Light', fontSize: '1.4em' }}>
            <span className="fontKarmaFuture" >{this.props.labels.LABEL_FOOTER}</span>
          </Footer>
        </Layout>
      </Spin>
    );
  }
}



export default TeamAuth;