import React, { Component } from 'react';
import { Row, Col, Layout, Card, Avatar } from 'antd';
import '../../../public/css/init.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';

const { Footer, Content } = Layout;

import bookImage from '../../../public/images/book-8-bits.png';

/**
 * Componente Responsável por realizar o cadastro inicial do usuário
 * 
 */

@observer
@autobind
class NoSupportBrowser extends Component {

  /**
   * Renderiza o componente
   */
  render() {
    return (
      <Layout className='container' >
        <Content className='contentCad'>
          <Row type='flex' justify='center' className="cardCenter">
            <Col xl={9} lg={12} md={24} sm={32}>
              <Card>
                <Row type='flex' justify='center'>
                  <Col><span className="fontKarmaFuture logoText">{this.props.labels.LABEL_GINCO}</span><img src={bookImage} className="imgLogo" alt="logo" /></Col>
                </Row>
                <Row>
                  <Col><span className="title">{this.props.labels.MSG_NO_BROWSER_SUPPORT}</span></Col>
                </Row>
                <Row className="hoverRow">
                  
                </Row>
              </Card>
            </Col>
          </Row>
        </Content>
        <Footer className='footerCad'>
          <span className="fontKarmaFuture" >{this.props.labels.LABEL_FOOTER}</span>
        </Footer>
      </Layout>
    );
  }
}



export default NoSupportBrowser;