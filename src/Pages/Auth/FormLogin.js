import React, { Component } from 'react';
import { Form, Input, Button, Row } from 'antd';
import '../../../public/css/register.css';
import autobind from 'autobind-decorator';
import { observer } from "mobx-react";
import DeathStar from 'react-deathStar';

const FormItem = Form.Item;

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
  },
};

/**
 * Componente Responsável por criar o formulário de cadastro
 * 
 */
@observer
@autobind
class FormLogin extends Component {

  /**
   * Construtor da classe
   */
  constructor() {
    super();
    this.state = {
      deathStar: DeathStar.getInstance(React),
      confirmDirty: false
    };
  }

  /**
   * Realiza as ações quando se monta o componente
   */
  componentDidMount() {
    this.props.centralCommand.executeAction('./Auth', 'getRulesOfLogin', [this.checkConfirm]);
  }

  /**
   * Método responsável por verficar se os campos informados estão corretamente preenchidos.
   * 
   * @param {object} rule 
   * @param {object} value 
   * @param {object} callback 
   */
  checkConfirm(rule, value, callback) {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  }

  /**
   *  Verifica se o campo é válido ao tirar o foco do mesmo e grava isso em um estado.
   * 
   * @param {event} e 
   */
  handleConfirmBlur(e) {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  }

  /**
   * Processo os dados do formulário para envio
   * 
   * @param {event} e 
   */
  handleForm(e) {
    e.preventDefault();

    this.props.store.loading = true;
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        this.props.centralCommand.executeAction('./Auth', 'login', [values.email, values.password]);
      } else {
        this.props.store.loading = false;
      }
    });
  }

  /**
   * Renderiza o componente
   */
  render() {
    const { getFieldDecorator } = this.props.form;
    const fontStyle = { fontFamily: 'Lato Regular', fontSize: '1.4em' };

    return (
      <Form onSubmit={this.handleForm}>
        <FormItem
          {...formItemLayout}
          label={<span style={fontStyle}>{this.props.labels.LABEL_EMAIL}</span>}
          hasFeedback
        >
          {this.props.store.dataRules.emailRules ? getFieldDecorator('email', { rules: this.props.store.dataRules.emailRules })(<Input />) : ''}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label={<span style={fontStyle}>{this.props.labels.LABEL_PASSWORD}</span>}
          hasFeedback>
          {this.props.store.dataRules.passwordRules ? getFieldDecorator('password', { rules: this.props.store.dataRules.passwordRules })(<Input type="password" />) : ''}
        </FormItem>
        <Row type="flex" justify="space-around" align="middle">
          <Button className='cadButton' style={{ fontFamily: 'Lato Regular', fontSize: '1.6em' }} htmlType="submit" size="large">{this.props.labels.LABEL_ENTER}</Button>
        </Row>
      </Form>
    );
  }
}

export default FormLogin;