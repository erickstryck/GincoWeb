import Constants from '../Util/Constants';
import Store from '../Util/Store';
import CentralCommand from './CentralCommand';
import Team from './Team';
import 'babel-polyfill';

var firebase;
/**
 * Classe responsável por prover os serviços de authenticação
 */
export default class Question {

	/**
	 * Construtor da classe
	 * 
	 * @param {object} firebase 
	 */
	constructor(firebaseContext) {
		firebase = firebaseContext;
		this.store = Store.getInstance();
	}

	/**
	 * Função responsável por retornar o numéro de perguntas que cada equipe deverá enviar
	 * 
	 * @param {number} teamNumber 
	 */
	static getQuestionsNumber(teamNumber, count = 0) {
		let divVal = teamNumber / 2;
		if (divVal > 1 && divVal % 2 == 0) {
			return Question.getQuestionsNumber(divVal, ++count);
		} else if (divVal == 1) {
			return ++count;
		} else return count;
	}

	/**
	 * Salva as perguntas no banco de dados do firebase
	 * @param {*} values 
	 * @param {*} indexAlternative 
	 */
	save(values, indexAlternative) {
		this.store.loadingUpdates = true;
		let objectQuestion = {};
		objectQuestion.enunciado = values.enunciado;
		objectQuestion.alternativas = [];

		let locationData = window.history.state.teamKeys;

		Object.keys(values).forEach((key, index) => {
			if (key == "alternativa_" + index) {
				let tempObj = {};
				tempObj.descricao = values[key];
				tempObj.isCorreta = index == indexAlternative;

				objectQuestion.alternativas.push(tempObj);
			}
		});

		firebase.database().ref('/professores/' + locationData.teacherKey + '/gincanas/' + locationData.eventKey + "/equipes/" + locationData.teamKey + "/perguntas").push(objectQuestion).then((result) => {
			(new Team(firebase)).update(Store.getInstance().team, true);
		}).catch(function (error) {
			Store.getInstance().loadingUpdates = false;
			CentralCommand.processError(error);
		});
	}

    /**
	 * Atualiza as perguntas no banco de dados do firebase
	 * 
	 * @param {*} values 
	 * @param {*} indexAlternative 
	 */
	update(values, indexAlternative, isValid, updateKey) {
		this.store.loadingUpdates = true;
		let objectQuestion = {};
		objectQuestion.enunciado = values.enunciado;
		if (this.store.isTeacher) {
			objectQuestion.validate = isValid;
			if (!isValid) {
				objectQuestion.note = values.note;
			} else {
				objectQuestion.note = "";
			}
		} else {
			objectQuestion.validate = "";
		}

		objectQuestion.alternativas = [];

		let locationData = window.history.state.teamKeys;

		Object.keys(values).forEach((key, index) => {
			if (key == "alternativa_" + index) {
				let tempObj = {};
				tempObj.descricao = values[key];
				tempObj.isCorreta = index == indexAlternative;

				objectQuestion.alternativas.push(tempObj);
			}
		});

		firebase.database().ref('/professores/' + locationData.teacherKey + '/gincanas/' + locationData.eventKey + "/equipes/" + locationData.teamKey + "/perguntas/" + updateKey).update(objectQuestion).then((result) => {
			(new Team(firebase)).update(Store.getInstance().team, true);
		}).catch(function (error) {
			Store.getInstance().loadingUpdates = false;
			CentralCommand.processError(error);
		});
	}

	/**
	 * Recupera as regras dos campos para o formulário de criação de gincana.
	 * 
	 * @param {object} checkConfirm 
	 */
	getRulesOfQuestion(checkConfirm) {
		let rules = {};
		let messages = Constants[this.store.lang];

		rules.question = [{
			type: 'string', message: messages.LABEL_REQUIRED
		}, {
			required: true, message: messages.LABEL_PLEASE_INSERT_QUESTION
		}, {
			validator: checkConfirm
		}, {
			max: 178, message: messages.MSG_WEAK_QUESTION
		}];


		rules.alternative = [{
			type: 'string', message: messages.LABEL_REQUIRED
		}, {
			required: true, message: messages.LABEL_PLEASE_INSERT_ALTERNATIVE
		}, {
			validator: checkConfirm
		}, {
			max: 120, message: messages.MSG_WEAK_ALTERNATIVE
		}];

		rules.note = [{
			type: 'string', message: messages.LABEL_REQUIRED
		}, {
			required: true, message: messages.LABEL_ADD_NOTE
		}, {
			validator: checkConfirm
		}, {
			max: 200, message: messages.MSG_WEAK_NOTE
		}];

		this.store.questionRules = rules;
	}

}