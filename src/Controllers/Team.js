import Constants from '../Util/Constants';
import Store from '../Util/Store';
import CentralCommand from './CentralCommand';
import 'babel-polyfill';
import Question from './Question';
import moment from 'moment';
import DateTimeServer from '../Util/DateTimeServer';
moment.locale('pt-BR');

var firebase;
/**
 * Classe responsável por prover os serviços de authenticação
 */
export default class Team {

  /**
   * Construtor da classe
   * 
   * @param {object} firebase 
   */
  constructor(firebaseContext) {
    firebase = firebaseContext;
    this.store = Store.getInstance();
  }

  /**
   * Cria uma chave de acesso para equipes na gincana.
   */
  static getTeamAcessKey() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + "_" + s4() + s4();
  }

  /**
  * Função responsável por verificar se o número de equipes está balanceado.
  * 
  * @param {number} teamNumber 
  */
  static validateTeamNumber(teamNumber) {
    let divVal = teamNumber / 2;
    if (divVal > 1 && divVal % 2 == 0) {
      return Team.validateTeamNumber(divVal);
    } else if (divVal <= 1) {
      return true;
    } else return false;
  }

  /**
   * Recupera o estado da equipe para validação
   */
  validateTeamStatus() {
    let hasValid = false;
    if (this.store.team.nome && this.store.team.message && this.store.team.image) {
      hasValid = true;
    }

    if (hasValid) {
      hasValid = this.store.updatedQuestionSize <= 0;
      if (hasValid) {
        for (let i = 0; i < this.store.questionData.length; i++) {
          if (!this.store.questionData[i].validate) {
            hasValid = false;
            break;
          }
        }
      }
    }

    return hasValid;
  }

  /**
   * Atualiza a equipe no banco de dados do firebase
   * 
   * @param {object} values 
   * @param {boolean} onlyStatus 
   */
  update(values, onlyStatus = false) {
    let locationData = window.history.state.teamKeys;

    if (onlyStatus) {
      values = { valid: this.validateTeamStatus() }
    } else {
      this.store.loadingUpdates = true;
      values.image = this.store.team.image;
      values.valid = this.validateTeamStatus();
      this.store.team.valid = values.valid;
      delete values.keys;

      if (!values.image) values.image = '';
    }
    this.store.hasTeamUpdates = false;
    firebase.database().ref('/professores/' + locationData.teacherKey + '/gincanas/' + locationData.eventKey + "/equipes/" + locationData.teamKey).update(values).then(() => {
      Store.getInstance().loadingUpdates = false;
    }).catch(function (error) {
      Store.getInstance().loadingUpdates = false;
      CentralCommand.processError(error);
    });
  }

  /**
   * Atualiza os estados das equipes quando há a inclusão ou remoção de uma equipe.
   * 
   * @param {string} eventKey 
   */
  static async updateTeamsStatus(eventKey, oldTeamsSize) {
    Store.getInstance().loadingUpdates = true;
    let oldNumberOfBalances = Team.getNumberOfBalancingTimes(oldTeamsSize);
    let oldQuestionsSize = (Question.getQuestionsNumber(oldTeamsSize - oldNumberOfBalances) * 3) + 3;

    let numberOfBalances = Team.getNumberOfBalancingTimes(Store.getInstance().accesses);
    var questionsSize = (Question.getQuestionsNumber(Store.getInstance().accesses - numberOfBalances) * 3) + 3;

    if (oldQuestionsSize != questionsSize) {
      await firebase.database().ref('/professores/' + Store.getInstance().user.uid + '/gincanas/' + eventKey + '/equipes').once('value', async (snapshot) => {
        let teams = snapshot.val();
        let teamsUpdate = {};
        if (teams) {
          for (let key in teams) {
            let isValid = false;
            let team = teams[key];
            if (team.nome && team.message && team.image) {
              isValid = true;
            }

            if (isValid) {
              isValid = Object.keys(team.perguntas).length >= questionsSize;

              if (isValid) {
                for (let questionKey in team.perguntas) {
                  if (!team.perguntas[questionKey].validate) {
                    isValid = false;
                    break;
                  }
                }
              }
            }

            if (typeof team.valid === 'boolean') {
              await firebase.database().ref('/professores/' + Store.getInstance().user.uid + '/gincanas/' + eventKey + "/equipes/" + key).update({ valid: isValid })
                .catch(function (error) {
                  Store.getInstance().loadingUpdates = false;
                  CentralCommand.processError(error);
                });
            }
          }

          Store.getInstance().loadingUpdates = false;
        }
      });
    } else {
      Store.getInstance().loadingUpdates = false;
    }
  }

	/**
   * Recupera o número de vezes que deverá ocorrer um balanceamento.
   */
  static getNumberOfBalancingTimes(teamSize) {
    let balanceTimes = 0;
    while (!Team.validateTeamNumber(teamSize)) {
      --teamSize;
      ++balanceTimes;
    }

    return balanceTimes;
  }

  /**
   * Recupera a imagem em formato base64
   * 
   * @param {Object} file 
   * @param {function} callback 
   */
  getBase64(file, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(file);
  }

  /**
   * Verifica de a imagem enviada é válida
   * 
   * @param {object} file 
   */
  validateImage(file) {
    let messages = Constants[this.store.lang];
    this.store.hasImageUploadError = false;

    const isJPG = file.type === 'image/jpeg';
    const isPNG = file.type === 'image/png';
    if (!isJPG && !isPNG) {
      this.store.hasImageUploadError = true;
      CentralCommand.processError(messages.MSG_INVALID_IMG);
    }

    const isLt2M = file.size / 1024 / 1024 < this.store.IMAGE_SIZE;
    if (!isLt2M) {
      this.store.hasImageUploadError = true;
      CentralCommand.processError(messages.MSG_INVALID_IMG_SIZE);
    }

    return (isJPG || isPNG) && isLt2M;
  }

  /**
   * Redimensiona a imagem para o padrão estipulado
   * @param {*} file 
   */
  imageResize(file) {
    this.getBase64(file, async (image64) => {
      var canvas = document.createElement("canvas");
      var ctx = canvas.getContext("2d");

      canvas.width = 800; // target width
      canvas.height = 600; // target height

      var image = new Image();
      image.onload = function (e) {
        ctx.drawImage(image,
          0, 0, image.width, image.height,
          0, 0, canvas.width, canvas.height
        );

        Store.getInstance().team.image = canvas.toDataURL();
        Store.getInstance().hasTeamUpdates = true;
        Store.getInstance().loading = false;
      };
      image.src = image64;
    });
  }

  /**
   * Realiza os procedimentos na imagem.
   * @param {Object} file 
   */
  processImage(file) {
    this.store.loading = true;
    this.imageResize(file);
  }

  /**
   * Recupera as regras dos campos para o formulário de criação de gincana.
   * 
   * @param {object} checkConfirm 
   */
  getRulesOfTeam(checkConfirm) {
    let rules = {};
    let messages = Constants[this.store.lang];

    rules.nameRules = [{
      type: 'string', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_TEAM_NAME
    }, {
      validator: checkConfirm
    }, {
      max: 23, message: messages.MSG_WEAK_TEAM_NAME
    }];


    rules.warCry = [{
      type: 'string', message: messages.LABEL_REQUIRED
    }, {
      required: true, message: messages.LABEL_PLEASE_INSERT_TEAM_WARCRY
    }, {
      validator: checkConfirm
    }, {
      max: 76, message: messages.MSG_WEAK_TEAM_WARCRY
    }];

    rules.student = [
      {
        type: 'string', message: messages.LABEL_REQUIRED
      }, {
        required: true, message: messages.LABEL_EMPTY_STUDENT
      }, {
        validator: checkConfirm
      }, {
        max: 30, message: messages.MSG_WEAK_STUDENT_NAME
      }];

    this.store.rules = rules;
  }

  /**
   * Verifica se já existe uma gincana cadastrada com o nome informado.
   */
  verifyIfTeamNameExist(name) {
    const teamAccessKeys = window.history.state.teamKeys;

    if (this.store.team.nome != name) {
      this.store.loading = true;
      return firebase.database().ref('/professores/' + teamAccessKeys.teacherKey + '/gincanas/' + teamAccessKeys.eventKey + '/equipes').once('value', (snapshot) => {
        let dataTeams = snapshot.val();
        if (dataTeams) {
          dataTeams = Object.values(dataTeams);
          let filtered = dataTeams.filter(team => {
            return team.nome == name;
          });

          let messages = Constants[Store.getInstance().lang];
          if (filtered.length) {
            Store.getInstance().notifyFieldValueExists = { validateStatus: "error", help: messages.LABEL_TEAM_NAME_EXIST };
          } else {
            Store.getInstance().notifyFieldValueExists = ''
          }
        }
        Store.getInstance().loading = false;
      });
    }
  }

  /**
   * Retorna o array contendo os alunos vinculadas a equipe
   * 
   * @param {object} team 
   */
  getArrayOfStudents(team) {
    if (team.alunos) {
      let tempAlunos = team.alunos;
      team.alunos = [];
      Object.keys(tempAlunos).forEach((key) => {
        team.alunos.push(tempAlunos[key]);
      });
    }

    return team;
  }

  /**
   * Recupera os dados da equipe para carregar os formulários
   * 
   * @param {string} eventKey
   */
  getTeamData(teamAccessKeys, createCardSlot) {
    this.store.loading = true;
    firebase.database().ref('/professores/' + teamAccessKeys.teacherKey + '/gincanas/' + teamAccessKeys.eventKey).on('value', (snapshot) => {
      let event = snapshot.val();
      if (event) {
        const nodeEnv = process.env.NODE_ENV || 'development';
        if (nodeEnv === 'production') {
          let time = DateTimeServer.getServerTime();
          console.log(time);
          Store.getInstance().expireQuestionCadPeriod = moment(time).isAfter(moment(event.dataLimitePerguntas + " 23:59", "DD/MM/YYYY HH:mm"));
        } else {
          Store.getInstance().expireQuestionCadPeriod = false;
        }
        event.key = teamAccessKeys.eventKey;
        Store.getInstance().gincanaElement = event;
        firebase.database().ref('/professores/' + teamAccessKeys.teacherKey + '/gincanas/' + teamAccessKeys.eventKey + "/equipes/" + teamAccessKeys.teamKey).on('value', (snapshotTeam) => {
          let team = snapshotTeam.val();
          if (team) {
            team = this.getArrayOfStudents(team);
            Store.getInstance().team = team;
            let teamsSize = Object.keys(event.equipes).length;
            let numberOfBalances = Team.getNumberOfBalancingTimes(teamsSize);
            var questionsSize = (Question.getQuestionsNumber(teamsSize - numberOfBalances) * 3) + 3;
            if (team.perguntas) {
              Store.getInstance().questions = [];
              Object.keys(team.perguntas).forEach(async (key) => {
                await firebase.database().ref('/professores/' + teamAccessKeys.teacherKey + '/gincanas/' + teamAccessKeys.eventKey + "/equipes/" + teamAccessKeys.teamKey + "/perguntas/" + key).on('value', (snapshotQuestions) => {
                  let questions = snapshotQuestions.val();

                  if (questions) {
                    questions.key = key;
                    Store.getInstance().questions.push(questions);
                  } else {
                    Store.getInstance().questions = [];
                  }
                });
              });
            }

            Store.getInstance().updatedQuestionSize = questionsSize - Store.getInstance().questions.length;
            createCardSlot();
          }
        });
      }

      Store.getInstance().loading = false;
    });
  }

  /**
   * Recupera os times para criação dos cards de visualização.
   * 
   * @param {string} eventKey
   * @param {function} createTeamAccessCard 
   */
  getTeamsOfEvent(eventKey, createTeamAccessCard) {
    this.store.loading = true;

    firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + eventKey + '/equipes').on('value', (snapshot) => {
      let teams = snapshot.val();
      let keyView = 0;
      if (teams) {
        for (let key in teams) {
          createTeamAccessCard(teams[key], { eventKey: eventKey, teamKey: key }, keyView == 0);
          ++keyView;
        }
      } else {
        createTeamAccessCard(null, null, null, true);
      }

      Store.getInstance().accesses = keyView;
      Store.getInstance().loading = false;
    });
  }

  /**
   * Exclui a equipe na base de dados.
   * 
   * @param {number} keyView 
   * @param {string} eventKey 
   * @param {string} teamKey
   */
  deleteTeam(eventKey, teamKey) {
    this.store.loadingUpdates = true;
    firebase.database().ref('/teamAccess').once('value', (access) => {
      access = access.val();
      if (access) {
        let revokeAccess = Object.keys(access).filter(key => {
          return access[key].teamKey == teamKey;
        });

        revokeAccess.forEach((keyTarget) => {
          firebase.database().ref('/teamAccess/' + keyTarget).remove();
        });

        var oldAccessSize = Store.getInstance().accesses;
        firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + eventKey + '/equipes/' + teamKey).remove(() => {
          Team.updateTeamsStatus(eventKey, oldAccessSize);
        });
      }
    });
  }

  /**
   * Salva os dados da equipe e gera um card com as informações
   * 
   * @param {string} eventKey 
   */
  saveAccessTeam(eventKey) {
    if (this.store.accesses < this.store.NUMBER_MAX_TEAM) {

      this.store.loadingUpdates = true;
      let messages = Constants[this.store.lang];

      var teamValues = {
        nome: messages.LABEL_DEFAULT_TEAM + ++this.store.accesses,
        accessPass: Team.getTeamAcessKey()
      }

      Team.updateTeamsStatus(eventKey, Store.getInstance().accesses - 1);

      firebase.database().ref('/professores/' + this.store.user.uid + '/gincanas/' + eventKey + '/equipes').push(teamValues).then(function (result) {
        let acccess = {
          teamKey: result.key,
          eventKey: eventKey,
          teacherKey: Store.getInstance().user.uid,
          accessPass: teamValues.accessPass
        }
        firebase.database().ref('/teamAccess').push(acccess).catch(function (error) {
          Store.getInstance().loadingUpdates = false;
          CentralCommand.processError(error);
        });
      }).catch(function (error) {
        Store.getInstance().loadingUpdates = false;
        CentralCommand.processError(error);
      });
    }
  }
}