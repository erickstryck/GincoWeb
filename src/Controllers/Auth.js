import CentralCommand from "./CentralCommand";
import Constants from "../Util/Constants";
import Store from "../Util/Store";

var firebase;
/**
 * Classe responsável por prover os serviços de authenticação
 */
export default class Auth {
  /**
   * Construtor da classe
   *
   * @param {object} firebase
   */
  constructor(firebaseContext) {
    firebase = firebaseContext;
    this.store = Store.getInstance();
  }

  /**
   * Realiza o cadastro inicial do usuário no sistema
   *
   * @param {string} email
   * @param {string} password
   */
  initRegisterUser(email, password) {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(
        () => {
          CentralCommand.getInstance().executeAction(
            "./Auth",
            "sendEmailVerification"
          );
          CentralCommand.getInstance().executeAction(
            "./Auth",
            "createInitialUserData"
          );
          Store.getInstance().initReg = true;
          Store.getInstance().loading = false;
        },
        error => {
          Store.getInstance().loading = false;
          CentralCommand.processError(error);
        }
      );
  }

  /**
   * Efetua login utilizando a rede do google
   */
  async googleLogin() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().useDeviceLanguage();
    await firebase
      .auth()
      .signInWithPopup(provider)
      .then(function(result) {
        Store.getInstance().user = result.user;
        CentralCommand.getInstance().executeAction(
          "./Auth",
          "createInitialUserData"
        );
        Store.getInstance().isTeacher = true;
        Store.getInstance().feedBackAccountBlock = true;
        window.history.pushState(null, "", "/home");
        window.location.reload();
      })
      .catch(function(error) {
        if (error.code == Constants.EMAIL_IN_USE_ACCOUNT) {
          CentralCommand.getInstance().executeAction("./Auth", "facebookLogin");
        } else console.error(error);
      });
  }

  /**
   * Efetua login utilizando a rede do facebook
   */
  async facebookLogin() {
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().useDeviceLanguage();
    await firebase
      .auth()
      .signInWithPopup(provider)
      .then(function(result) {
        Store.getInstance().user = result.user;
        CentralCommand.getInstance().executeAction(
          "./Auth",
          "createInitialUserData"
        );
        Store.getInstance().isTeacher = true;
        Store.getInstance().feedBackAccountBlock = true;
        window.history.pushState(null, "", "/home");
        window.location.reload();
      })
      .catch(function(error) {
        if (error.code == Constants.EMAIL_IN_USE_ACCOUNT) {
          CentralCommand.getInstance().executeAction("./Auth", "googleLogin");
        } else console.error(error);
      });
  }

  /**
   * Procura a chave no repositótio e autêntica a equipe
   */
  async authTeam(accessKey) {
    this.store.loading = true;
    await this.login(this.store.TEAM_ACCESS_USER, this.store.TEAM_ACCESS_PASS);
    firebase
      .database()
      .ref("/teamAccess")
      .on("value", snapshot => {
        let accessKeyData = snapshot.val();
        let resultAccess = Object.keys(accessKeyData).filter(key => {
          return accessKeyData[key].accessPass == accessKey;
        });

        if (resultAccess.length) {
          window.history.pushState(
            { teamKeys: accessKeyData[resultAccess[0]] },
            "",
            "/teamform"
          );
          window.location.reload();
        } else {
          Store.getInstance().loading = false;
          firebase.auth().signOut();
          CentralCommand.processError({ code: Constants.INVALID_ACCESS_KEY });
        }
      });
  }

  /**
   *
   * Recupera as regras dos campos para a página cadastro inícial 'InitRegister'.
   */
  getRulesOfInitRegister() {
    let rules = {};
    let messages = Constants[this.store.lang];

    rules.emailRules = [
      {
        type: "email",
        message: messages.LABEL_REQUIRED
      },
      {
        required: true,
        message: messages.LABEL_PLEASE_INSERT_MAIL
      }
    ];

    rules.passwordRules = [
      {
        required: true,
        message: messages.LABEL_PLEASE_INSERT_PASSWORD
      },
      {
        min: 6,
        message: messages.MSG_WEAK_PASSWORD
      },
      {
        max: 16,
        message: messages.MSG_WEAK_PASSWORD
      }
    ];

    this.store.rules = rules;
  }

  /**
   * Recupera as regras dos campos para a página de login 'Login'.
   *
   * @param {object} checkConfirm
   */
  getRulesOfLogin(checkConfirm) {
    let rules = {};
    let messages = Constants[this.store.lang];

    rules.emailRules = [
      {
        type: "email",
        message: messages.LABEL_REQUIRED
      },
      {
        required: true,
        message: messages.LABEL_PLEASE_INSERT_MAIL
      }
    ];

    rules.passwordRules = [
      {
        required: true,
        message: messages.LABEL_PLEASE_INSERT_PASSWORD
      },
      {
        validator: checkConfirm
      },
      {
        min: 6,
        message: messages.MSG_WEAK_PASSWORD
      },
      {
        max: 16,
        message: messages.MSG_WEAK_PASSWORD
      }
    ];

    this.store.rules = rules;
  }

  /**
   * Recupera as regras dos campos para o modal de recuperação de senha
   */
  getRulesOfPasswordRecovery() {
    let rules = {};
    let messages = Constants[this.store.lang];

    rules.emailRules = [
      {
        type: "email",
        message: messages.LABEL_REQUIRED
      },
      {
        required: true,
        message: messages.LABEL_PLEASE_INSERT_MAIL
      }
    ];

    this.store.rules = rules;
  }

  /**
   * Cria o objeto json no firebase que será usada para armazenar as estrutura de dados do usuário recem cadastrado.
   *
   * @param {string} uid
   */
  createInitialUserData() {
    let user = firebase.auth().currentUser;

    let userData = {};
    userData[user.uid] = {
      profile: { lang: "PT_BR" },
      nome: user.displayName,
      email: user.email
    };
    this.store.lang = userData[user.uid].profile.lang;
    firebase
      .database()
      .ref("/professores/")
      .update(userData)
      .then(function(result) {
        Store.getInstance().loading = false;
      })
      .catch(function(error) {
        Store.getInstance().loading = false;
        CentralCommand.processError(error);
      });
  }

  /**
   * Envia um e-mail de verificação de conta para o usuário recem cadastrado
   */
  sendEmailVerification() {
    let user = firebase.auth().currentUser;

    user.sendEmailVerification().then(
      () => {
        Store.getInstance().mailVerificationSend = true;
      },
      error => {
        CentralCommand.processError(error);
      }
    );
  }

  /**
   * Realiza a autenticação do usuário no sistema por meio do firebase
   *
   * @param {string} email
   * @param {string} password
   */
  login(email, password) {
    return firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(
        () => {
          Store.getInstance().isTeacher = true;
          Store.getInstance().loading = false;
          Store.getInstance().feedBackAccountBlock = true;
        },
        error => {
          Store.getInstance().loading = false;
          CentralCommand.processError(error);
        }
      );
  }

  /**
   * Desloga o usuário do sistema.
   *
   */
  logout() {
    firebase
      .auth()
      .signOut()
      .catch(error => {
        console.log(error);
        CentralCommand.processError(error);
      });

    window.history.pushState(null, "", "/init");
    window.location.reload();
  }

  /**
   * Desloga o usuário do sistema.
   *
   */
  resetPassword(emailAddress) {
    firebase
      .auth()
      .sendPasswordResetEmail(emailAddress)
      .then(
        () => {
          Store.getInstance().mailRecoverySend = true;
        },
        error => {
          CentralCommand.processError(error);
        }
      );
  }
}
